/**
 * Created by su on 3/7/16.
 */
var map = new L.Map('Map', {fullscreenControl: true,center: new L.LatLng(36.78459,34.58875), zoom: 16});

var pointList = [];
L.tileLayer('http://{s}.tile.osm.org/{z}/{x}/{y}.png', {
    attribution: '&copy; <a href="http://www.meridya.com">Meridya</a> Engineering',

}).addTo(map);

var polygon = new L.polygon([
    [36.78720, 34.58832],
    [36.78478, 34.58945],
    [36.78292, 34.59168],
    [36.78255, 34.59046],
    [36.78552, 34.58637],
]).addTo(map);
var output;
function historyLocations(){
  //alert($(this).attr('value'));
  $.get( "/devices/histloc/" + $(this).attr('value') ,
    function( data ) {
      var pointList = [];
      var jsonData = JSON.parse(data);
      for(row in jsonData)
      {
        if(jsonData[row].lat > 0 && jsonData[row].lon>0)
        {
          pointList.push(new L.LatLng(jsonData[row].lat,jsonData[row].lon));
        }


      }
      var firstpolyline = new L.polyline(pointList, {
      color: 'red'
      });
      firstpolyline.addTo(map);
      map.fitBounds(firstpolyline.getBounds());
      //$( ".result" ).html( data );
      //alert( "Load was performed." );
    });
}
$( document ).ready(function() {
  //------------------------------------------------
  theGrid = $('#thegrid').DataTable({
      "processing": true,
      "serverSide": true,
      "ordering": false,
      "responsive": true,
      "ajax": "/devices/history-grid/"+$("#deviceId").val(),
      "columnDefs": [
          {
              "render": function ( data, type, row ) {
                  return '<a href="#" id="getHistory" value="'+row[1]+'"  class="btn btn-success">Göster</a>'+
                      '<script type="text/javascript"> $("#getHistory").on("click",historyLocations);</script>';
              },
              "targets": 5+1
          },
      ]
  });
});
