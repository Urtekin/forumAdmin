/**
 * Created by su on 3/7/16.
 */
var map = new L.Map('Map', {fullscreenControl: true,center: new L.LatLng(36.78459,34.58875), zoom: 16});

var pointList = [];
L.tileLayer('http://{s}.tile.osm.org/{z}/{x}/{y}.png', {
    attribution: '&copy; <a href="http://www.meridya.com">Meridya</a> Engineering',

}).addTo(map);

var polygon = new L.polygon([
    [36.78720, 34.58832],
    [36.78478, 34.58945],
    [36.78292, 34.59168],
    [36.78255, 34.59046],
    [36.78552, 34.58637],
]).addTo(map);
var output;
var tempPoint = new L.marker([36.78552, 34.58637]);
setInterval(function(){
  $.get( "/devices/lastloc/" + $("#deviceId").val() ,
  function(incoming){
    var jsonData = JSON.parse(incoming);
    if(tempPoint>0)
      map.removeLayer(tempPoint);

      tempPoint = new L.marker([jsonData.lat,jsonData.lon]);
      tempPoint.addTo(map);
    });
 }, 8000);
