/**
 * Created by su on 3/7/16.
 */
var map = new L.Map('Map', {fullscreenControl: true,center: new L.LatLng(36.78459,34.58875), zoom: 16});

var pointList = [];
L.tileLayer('http://{s}.tile.osm.org/{z}/{x}/{y}.png', {
    attribution: '&copy; <a href="http://www.meridya.com">Meridya</a> Engineering',

}).addTo(map);

var polygon = new L.polygon([
    [36.78720, 34.58832],
    [36.78478, 34.58945],
    [36.78292, 34.59168],
    [36.78255, 34.59046],
    [36.78552, 34.58637],
]).addTo(map);
var wsUri = "ws://54.191.135.135:5006";
var output;
websocket = new WebSocket(wsUri);
websocket.onopen = function(evt) { onOpen(evt) };
websocket.onclose = function(evt) { onClose(evt) };
websocket.onmessage = function(evt) { onMessage(evt) };
websocket.onerror = function(evt) { onError(evt) };
//websocket.connect();
function onOpen(evt)
{
    //alert("Connected");
    //writeToScreen("CONNECTED");
    //doSend("WebSocket rocks");
}
function onClose(evt)
{
    alert("DISCONNECTED");
    //writeToScreen("DISCONNECTED");
}
function onMessage(evt)
{
    //writeToScreen('<span style="color: blue;">RESPONSE: ' + evt.data+'</span>');
    var point = JSON.parse(evt.data);


    var tempPoint = new L.marker([point.lat , point.lon]);
    //if(pointList[point.imei] !==null||pointList[point.imei] !=="") {
    if(point.imei in pointList){
        map.removeLayer(pointList[point.imei]);
    }
    pointList[point.imei] = tempPoint;
    pointList[point.imei].addTo(map);
    /*
    map.eachLayer(function (marker) {
        map.removeLayer(marker);
    });
    *//*
    for(point in pointList)
    {
        map.removeLayer(pointList[point]);
        pointList[point].addTo(map);
    }*/
    //alert(evt.data);
    //websocket.close();
}
function onError(evt)
{
    alert("Hata: " + evt.data);
    //writeToScreen('<span style="color: red;">ERROR:</span> ' + evt.data);
}
