@extends('adminMaster')

@section('content')



<h2 class="page-header">Device</h2>

<div class="panel panel-default">
    <div class="panel-heading">
        View Device    </div>

    <div class="panel-body">
                
        <form action="{{ url('/devices') }}" method="POST" class="form-horizontal">
                
        <div class="form-group">
            <label for="id" class="col-sm-3 control-label">Id</label>
            <div class="col-sm-6">
                <input type="text" name="id" id="id" class="form-control" value="{{$model['id'] or ''}}" readonly="readonly">
            </div>
        </div>
        
                
        <div class="form-group">
            <label for="imei" class="col-sm-3 control-label">Imei</label>
            <div class="col-sm-6">
                <input type="text" name="imei" id="imei" class="form-control" value="{{$model['imei'] or ''}}" readonly="readonly">
            </div>
        </div>
        
                
        <div class="form-group">
            <label for="deviceId" class="col-sm-3 control-label">DeviceId</label>
            <div class="col-sm-6">
                <input type="text" name="deviceId" id="deviceId" class="form-control" value="{{$model['deviceId'] or ''}}" readonly="readonly">
            </div>
        </div>
        
        
        <div class="form-group">
            <div class="col-sm-offset-3 col-sm-6">
                <a class="btn btn-default" href="{{ url('/devices') }}"><i class="glyphicon glyphicon-chevron-left"></i> Back</a>
            </div>
        </div>

        </form>
    

    </div>
</div>







@endsection