@extends('adminMaster')

@section('content')


<h2 class="page-header">{{ ucfirst('devices') }}</h2>

<div class="panel panel-default">
    <div class="panel-heading">
        List of {{ ucfirst('devices') }}
    </div>

    <div class="panel-body">
        <div class="">
            <table class="table table-striped" id="thegrid">
              <thead>
                <tr>
                                        <th>Id</th>
                                        <th>Imei</th>
                                        <th>DeviceId</th>
                                        <th style="width:50px"></th>
                                        <th style="width:50px"></th>
                                        <th style="width:50px"></th>
                </tr>
              </thead>
              <tbody>
              </tbody>
            </table>
        </div>
        <a href="{{url('devices/add')}}" class="btn btn-primary" role="button">Add device</a>
    </div>
</div>




@endsection



@section('scripts')
    <script type="text/javascript">
        var theGrid = null;
        $(document).ready(function(){

            theGrid = $('#thegrid').DataTable({
                "processing": true,
                "serverSide": true,
                "ordering": false,
                "responsive": true,
                "ajax": "{{url('devices/grid')}}",
                "columnDefs": [
                    {
                        "render": function ( data, type, row ) {
                            return '<a href="{{url('devices/show')}}/'+row[0]+'">'+data +'</a>';
                        },
                        "targets": 1
                    },
                    {
                        "render": function ( data, type, row ) {
                            return '<a href="{{url('devices/whereis')}}/'+row[0]+'" class="btn btn-primary">Nerede</a>';
                        },
                        "targets": 3                    },
                    {
                        "render": function ( data, type, row ) {
                            return '<a href="{{url('devices/assignee')}}/'+row[0]+'" onclick="return getassignee('+row[0]+')" class="btn btn-info">Atama</a>';
                        },
                        "targets": 3+1
                    },
                    {
                        "render": function ( data, type, row ) {
                            return '<a href="{{url('devices/history')}}/'+row[0]+'" onclick="return getHistory('+row[0]+')" class="btn btn-success">Geçmiş</a>';
                        },
                        "targets": 5
                    },
                ]
            });
        });
        function doDelete(id) {
            if(confirm('You really want to delete this record?')) {
               $.ajax('{{url('devices/delete')}}/'+id).success(function() {
                theGrid.ajax.reload();
               });

            }
            return false;
        }
    </script>
@endsection
