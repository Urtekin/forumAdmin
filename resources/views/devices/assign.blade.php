@extends('adminMaster')

@section('content')


<h2 class="page-header">Device</h2>

<div class="panel panel-default">
    <div class="panel-heading">
        Add/Modify Device    </div>

    <div class="panel-body">

        <form action="{{ url('/devices') }}/assign/{{$device['id']}}" method="POST" class="form-horizontal">
            {{ csrf_field() }}
            <div class="form-group">
                <label for="imei" class="col-sm-3 control-label">Imei</label>
                <div class="col-sm-6">
                    <input type="text" name="imei" id="imei" class="form-control" value="{{$device['imei'] or ''}}" readonly="readonly">
                </div>
            </div>
            <div class="form-group">
                <label for="deviceId" class="col-sm-3 control-label">Cihaz Numarası</label>
                <div class="col-sm-6">
                    <input type="text" name="deviceId" id="deviceId" class="form-control" value="{{$device['deviceId'] or ''}}" readonly="readonly">
                </div>
            </div>
            <div class="form-group">
                <label for="kimlikno" class="col-sm-3 control-label">Kimlik Numarası</label>
                <div class="col-sm-6">
                    <input type="text" name="kimlikno" id="kimlikno" class="form-control" value="{{$assignement['kimlikNo'] or ''}}"
                    @if ($assignement['kimlikNo'])
                      readonly="readonly"
                    @endif
                    >
                </div>
            </div>
            <div class="form-group">
                <label for="name" class="col-sm-3 control-label">İsim Soyisim</label>
                <div class="col-sm-6">
                    <input type="text" name="name" id="name" class="form-control" value="{{$assignement['name'] or ''}}"
                    @if ($assignement['kimlikNo'])
                      readonly="readonly"
                    @endif
                    >
                </div>
            </div>

            <div class="form-group">
                <div class="col-sm-offset-3 col-sm-6">
                    <button type="submit" class="btn btn-success"
                    @if ($assignement['kimlikNo'])
                        disabled
                    @endif>
                        <i class="fa fa-plus"></i> Ata
                    </button>
                    <a class="btn btn-danger" href="{{ url('/devices/deleteassign')}}/{{$device['id']}}"
                    @if (!$assignement['kimlikNo'])
                        style="pointer-events: none;cursor: default;"
                        disabled
                    @endif>
                    <i class="glyphicon glyphicon-minus"></i> Bitir</a>
                    <a class="btn btn-default" href="{{ url('/devices') }}"><i class="glyphicon glyphicon-chevron-left"></i> Back</a>
                </div>
            </div>
        </form>

    </div>
</div>






@endsection
