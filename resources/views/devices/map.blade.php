@extends('adminMaster')
@section('title')
	Forum Harita
@endsection
@section('content')
<link href="/css/map.css" rel="stylesheet">
<link rel="stylesheet" href="/css/leaflet.css" />
<div class="panel panel-default">
    <div class="panel-heading">
        Add/Modify Device    </div>

    <div class="panel-body">

        <form action="" class="form-horizontal">
            {{ csrf_field() }}

            <div class="form-group">
                <label for="id" class="col-sm-3 control-label">Id</label>
                <div class="col-sm-6">
                    <input type="text" name="id" id="id" class="form-control" value="{{$model['id'] or ''}}" readonly="readonly">
                </div>
            </div>
                                                                                                            <div class="form-group">
                <label for="imei" class="col-sm-3 control-label">Imei</label>
                <div class="col-sm-6">
                    <input type="text" name="imei" id="imei" class="form-control" value="{{$model['imei'] or ''}}">
                </div>
            </div>
            <div class="form-group">
                <label for="deviceId" class="col-sm-3 control-label">DeviceId</label>
                <div class="col-sm-6">
                    <input type="text" name="deviceId" id="deviceId" class="form-control" value="{{$model['deviceId'] or ''}}">
                </div>
            </div>
            <div class="form-group">
                <div class="col-sm-offset-3 col-sm-6">
                    <a class="btn btn-default" href="{{ url('/devices') }}"><i class="glyphicon glyphicon-chevron-left"></i> Geri</a>
                </div>
            </div>
        </form>

    </div>
<div class="row">
    <div class="col-lg-12">
        <h1 class="page-header">Harita</h1>
    </div>
    <!-- /.col-lg-12 -->
</div>

<div class="panel panel-default">
    <div class="panel-heading">
        Harita    </div>

    <div class="panel-body">
        <div class="col-lg-12">
            <div id="Map"></div>
        </div>
        <!-- /.col-lg-12 -->
    </div>
  </div>


<script src="/js/leaflet.js"></script>
<script src="/js/map.js"></script>
@endsection
