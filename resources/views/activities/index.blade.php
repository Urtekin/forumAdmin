@extends('adminMaster')

@section('content')


<h2 class="page-header">{{ ucfirst('activities') }}</h2>

<div class="panel panel-default">
    <div class="panel-heading">
        List of {{ ucfirst('activities') }}
    </div>

    <div class="panel-body">
        <div class="">
            <table class="table table-striped" id="thegrid">
              <thead>
                <tr>
                                        <th>Id</th>
                                        <th>Title</th>
                                        <th>Content</th>
                                        <th>OntheAir</th>
                                        <th>StartDate</th>
                                        <th>EndDate</th>
                                        <th>Created At</th>
                                        <th>Updated At</th>
                                        <th style="width:50px"></th>
                    <th style="width:50px"></th>
                </tr>
              </thead>
              <tbody>
              </tbody>
            </table>
        </div>
        <a href="{{url('activities/add')}}" class="btn btn-primary" role="button">Add activity</a>
    </div>
</div>




@endsection



@section('scripts')
    <script type="text/javascript">
        var theGrid = null;
        $(document).ready(function(){
            theGrid = $('#thegrid').DataTable({
                "processing": true,
          s      "serverSide": true,
                "ordering": false,
                "responsive": true,
                "ajax": "{{url('activities/grid')}}",
                "columnDefs": [
                    {
                        "render": function ( data, type, row ) {
                            return '<a href="{{url('activities/show')}}/'+row[0]+'">'+data +'</a>';
                        },
                        "targets": 1
                    },
                    {
                        "render": function ( data, type, row ) {
                            return '<a href="{{url('activities/update')}}/'+row[0]+'" class="btn btn-default">Update</a>';
                        },
                        "targets": 8                    },
                    {
                        "render": function ( data, type, row ) {
                            return '<a href="{{url('activities/delete')}}" onclick="return doDelete('+row[0]+')" class="btn btn-danger">Delete</a>';
                        },
                        "targets": 8+1
                    },
                ]
            });
        });
        function doDelete(id) {
            if(confirm('You really want to delete this record?')) {
               $.ajax('{{url('activities/delete')}}/'+id).success(function() {
                theGrid.ajax.reload();
               });

            }
            return false;
        }
    </script>
@endsection
