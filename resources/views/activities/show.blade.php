@extends('adminMaster')

@section('content')



<h2 class="page-header">Activity</h2>

<div class="panel panel-default">
    <div class="panel-heading">
        View Activity    </div>

    <div class="panel-body">
                
        <form action="{{ url('/activities') }}" method="POST" class="form-horizontal">
                
        <div class="form-group">
            <label for="id" class="col-sm-3 control-label">Id</label>
            <div class="col-sm-6">
                <input type="text" name="id" id="id" class="form-control" value="{{$model['id'] or ''}}" readonly="readonly">
            </div>
        </div>
        
                
        <div class="form-group">
            <label for="title" class="col-sm-3 control-label">Title</label>
            <div class="col-sm-6">
                <input type="text" name="title" id="title" class="form-control" value="{{$model['title'] or ''}}" readonly="readonly">
            </div>
        </div>
        
                
        <div class="form-group">
            <label for="content" class="col-sm-3 control-label">Content</label>
            <div class="col-sm-6">
                <input type="text" name="content" id="content" class="form-control" value="{{$model['content'] or ''}}" readonly="readonly">
            </div>
        </div>
        
                
        <div class="form-group">
            <label for="ontheAir" class="col-sm-3 control-label">OntheAir</label>
            <div class="col-sm-6">
                <input type="text" name="ontheAir" id="ontheAir" class="form-control" value="{{$model['ontheAir'] or ''}}" readonly="readonly">
            </div>
        </div>
        
                
        <div class="form-group">
            <label for="startDate" class="col-sm-3 control-label">StartDate</label>
            <div class="col-sm-6">
                <input type="text" name="startDate" id="startDate" class="form-control" value="{{$model['startDate'] or ''}}" readonly="readonly">
            </div>
        </div>
        
                
        <div class="form-group">
            <label for="endDate" class="col-sm-3 control-label">EndDate</label>
            <div class="col-sm-6">
                <input type="text" name="endDate" id="endDate" class="form-control" value="{{$model['endDate'] or ''}}" readonly="readonly">
            </div>
        </div>
        
                
        <div class="form-group">
            <label for="created_at" class="col-sm-3 control-label">Created At</label>
            <div class="col-sm-6">
                <input type="text" name="created_at" id="created_at" class="form-control" value="{{$model['created_at'] or ''}}" readonly="readonly">
            </div>
        </div>
        
                
        <div class="form-group">
            <label for="updated_at" class="col-sm-3 control-label">Updated At</label>
            <div class="col-sm-6">
                <input type="text" name="updated_at" id="updated_at" class="form-control" value="{{$model['updated_at'] or ''}}" readonly="readonly">
            </div>
        </div>
        
        
        <div class="form-group">
            <div class="col-sm-offset-3 col-sm-6">
                <a class="btn btn-default" href="{{ url('/activities') }}"><i class="glyphicon glyphicon-chevron-left"></i> Back</a>
            </div>
        </div>

        </form>
    

    </div>
</div>







@endsection