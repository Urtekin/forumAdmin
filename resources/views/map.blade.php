@extends('adminMaster')
@section('title')
	Forum Harita
@endsection
@section('content')
<link href="css/map.css" rel="stylesheet">
<link rel="stylesheet" href="css/leaflet.css" />
<div class="row">
    <div class="col-lg-12">
        <h1 class="page-header">Harita</h1>
    </div>
    <!-- /.col-lg-12 -->
</div>
<div class="row">
    <div class="col-lg-12">
        <div id="Map"></div>
    </div>
    <!-- /.col-lg-12 -->
</div>


<script src="js/leaflet.js"></script>
<script src="js/map.js"></script>
@endsection