@extends('adminMaster')

@section('content')



<h2 class="page-header">Session</h2>

<div class="panel panel-default">
    <div class="panel-heading">
        View Session    </div>

    <div class="panel-body">
                
        <form action="{{ url('/sessions') }}" method="POST" class="form-horizontal">
                
        <div class="form-group">
            <label for="hour" class="col-sm-3 control-label">Hour</label>
            <div class="col-sm-6">
                <input type="text" name="hour" id="hour" class="form-control" value="{{$model['hour'] or ''}}" readonly="readonly">
            </div>
        </div>
        
                
        <div class="form-group">
            <label for="movieId" class="col-sm-3 control-label">MovieId</label>
            <div class="col-sm-6">
                <input type="text" name="movieId" id="movieId" class="form-control" value="{{$model['movieId'] or ''}}" readonly="readonly">
            </div>
        </div>
        
                
        <div class="form-group">
            <label for="saloon" class="col-sm-3 control-label">Saloon</label>
            <div class="col-sm-6">
                <input type="text" name="saloon" id="saloon" class="form-control" value="{{$model['saloon'] or ''}}" readonly="readonly">
            </div>
        </div>
        
        
        <div class="form-group">
            <div class="col-sm-offset-3 col-sm-6">
                <a class="btn btn-default" href="{{ url('/sessions') }}"><i class="glyphicon glyphicon-chevron-left"></i> Back</a>
            </div>
        </div>

        </form>
    

    </div>
</div>







@endsection