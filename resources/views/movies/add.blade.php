@extends('adminMaster')

@section('content')


<h2 class="page-header">Movie</h2>

<div class="panel panel-default">
    <div class="panel-heading">
        Add/Modify Movie    </div>

    <div class="panel-body">
                
        <form action="{{ url('/movies/save') }}" method="POST" class="form-horizontal">
            {{ csrf_field() }}

                                    <div class="form-group">
                <label for="id" class="col-sm-3 control-label">Id</label>
                <div class="col-sm-6">
                    <input type="text" name="id" id="id" class="form-control" value="{{$model['id'] or ''}}" readonly="readonly">
                </div>
            </div>
                                                                                                            <div class="form-group">
                <label for="name" class="col-sm-3 control-label">Name</label>
                <div class="col-sm-6">
                    <input type="text" name="name" id="name" class="form-control" value="{{$model['name'] or ''}}">
                </div>
            </div>
                                                                                                                        <div class="form-group">
                <label for="StartDate" class="col-sm-3 control-label">StartDate</label>
                <div class="col-sm-3">
                    <input type="date" name="StartDate" id="StartDate" class="form-control" value="{{$model['StartDate'] or ''}}">
                </div>
            </div>
                                                                                    <div class="form-group">
                <label for="active" class="col-sm-3 control-label">Active</label>
                <div class="col-sm-2">
                    <input type="number" name="active" id="active" class="form-control" value="{{$model['active'] or ''}}">
                </div>
            </div>
                                                
            <div class="form-group">
                <div class="col-sm-offset-3 col-sm-6">
                    <button type="submit" class="btn btn-success">
                        <i class="fa fa-plus"></i> Save
                    </button> 
                    <a class="btn btn-default" href="{{ url('/movies') }}"><i class="glyphicon glyphicon-chevron-left"></i> Back</a>
                </div>
            </div>
        </form>

    </div>
</div>






@endsection