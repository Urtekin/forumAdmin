@extends('adminMaster')

@section('content')


<h2 class="page-header">Harita</h2>

<div class="panel panel-default">
    <div class="panel-heading">
        Harita    </div>

    <div class="panel-body">


      <link href="/css/map.css" rel="stylesheet">
      <link rel="stylesheet" href="/css/leaflet.css" />
      <div class="row">
          <div class="col-lg-12">
              <h1 class="page-header">Harita</h1>
          </div>
          <!-- /.col-lg-12 -->
      </div>
      <div class="row">
          <div class="col-lg-12">
              <div id="Map"></div>
          </div>
          <!-- /.col-lg-12 -->
      </div>


      <script src="/js/leaflet.js"></script>
      <script src="/js/historyMap.js"></script>


        <form action="{{ url('/device_assignements/save') }}" method="POST" class="form-horizontal">
            {{ csrf_field() }}

            <div class="form-group">
                <label for="imei" class="col-sm-3 control-label">Imei</label>
                <div class="col-sm-6">
                    <input type="text" name="imei" id="imei" class="form-control" value="{{$model['imei'] or ''}}" readonly="readonly">
                </div>
            </div>
            <div class="form-group">
                <label for="kimlikNo" class="col-sm-3 control-label">Kimlik Numarası</label>
                <div class="col-sm-6">
                    <input type="text" name="kimlikNo" id="kimlikNo" class="form-control" value="{{$model['kimlikNo'] or ''}}" readonly="readonly">
                </div>
            </div>
            <div class="form-group">
                <label for="name" class="col-sm-3 control-label">İsim</label>
                <div class="col-sm-6">
                    <input type="text" name="name" id="name" class="form-control" value="{{$model['name'] or ''}}" readonly="readonly">
                </div>
            </div>
            <div class="form-group">
                <label for="startDate" class="col-sm-3 control-label">Başlangıç Tarihi</label>
                <div class="col-sm-3">
                    <input type="text" name="startDate" id="startDate" class="form-control" value="{{$model['startDate'] or ''}}" readonly="readonly">
                </div>
            </div>
            <div class="form-group">
                <label for="endDate" class="col-sm-3 control-label">Bitiş Tarihi</label>
                <div class="col-sm-3">
                    <input type="text" name="endDate" id="endDate" class="form-control" value="{{$model['endDate'] or ''}}" readonly="readonly">
                </div>
            </div>
            <div class="form-group">
                <label for="deviceId" class="col-sm-3 control-label">DeviceId</label>
                <div class="col-sm-6">
                    <input type="text" name="deviceId" id="deviceId" class="form-control" value="{{$model['deviceId'] or ''}}" readonly="readonly">
                </div>
            </div>
        </form>

    </div>
</div>
@endsection
