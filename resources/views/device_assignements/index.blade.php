@extends('adminMaster')

@section('content')


<h2 class="page-header">{{ ucfirst('device_assignements') }}</h2>

<div class="panel panel-default">
    <div class="panel-heading">
        List of {{ ucfirst('device_assignements') }}
    </div>

    <div class="panel-body">
        <div class="">
            <table class="table table-striped" id="thegrid">
              <thead>
                <tr>
                                        <th>Id</th>
                                        <th>Imei</th>
                                        <th>KimlikNo</th>
                                        <th>Name</th>
                                        <th>StartDate</th>
                                        <th>EndDate</th>
                                        <th>DeviceId</th>
                                        <th style="width:50px"></th>
                </tr>
              </thead>
              <tbody>
              </tbody>
            </table>
        </div>
    </div>
</div>




@endsection



@section('scripts')
    <script type="text/javascript">
        var theGrid = null;
        $(document).ready(function(){
            theGrid = $('#thegrid').DataTable({
                "processing": true,
                "serverSide": true,
                "ordering": false,
                "responsive": true,
                "ajax": "{{url('device_assignements/grid')}}",
                "columnDefs": [
                    {
                        "render": function ( data, type, row ) {
                            return '<a href="{{url('device_assignements/show')}}/'+row[0]+'">'+data +'</a>';
                        },
                        "targets": 1
                    },
                    {
                        "render": function ( data, type, row ) {
                            return '<a href="{{url('device_assignements/update')}}/'+row[0]+'" class="btn btn-default">Göster</a>';
                        },
                        "targets": 7
                    },
                ]
            });
        });
        function doDelete(id) {
            if(confirm('You really want to delete this record?')) {
               $.ajax('{{url('device_assignements/delete')}}/'+id).success(function() {
                theGrid.ajax.reload();
               });

            }
            return false;
        }
    </script>
@endsection
