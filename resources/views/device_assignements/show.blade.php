@extends('adminMaster')

@section('content')



<h2 class="page-header">Device_assignement</h2>

<div class="panel panel-default">
    <div class="panel-heading">
        View Device_assignement    </div>

    <div class="panel-body">
                
        <form action="{{ url('/device_assignements') }}" method="POST" class="form-horizontal">
                
        <div class="form-group">
            <label for="id" class="col-sm-3 control-label">Id</label>
            <div class="col-sm-6">
                <input type="text" name="id" id="id" class="form-control" value="{{$model['id'] or ''}}" readonly="readonly">
            </div>
        </div>
        
                
        <div class="form-group">
            <label for="imei" class="col-sm-3 control-label">Imei</label>
            <div class="col-sm-6">
                <input type="text" name="imei" id="imei" class="form-control" value="{{$model['imei'] or ''}}" readonly="readonly">
            </div>
        </div>
        
                
        <div class="form-group">
            <label for="kimlikNo" class="col-sm-3 control-label">KimlikNo</label>
            <div class="col-sm-6">
                <input type="text" name="kimlikNo" id="kimlikNo" class="form-control" value="{{$model['kimlikNo'] or ''}}" readonly="readonly">
            </div>
        </div>
        
                
        <div class="form-group">
            <label for="name" class="col-sm-3 control-label">Name</label>
            <div class="col-sm-6">
                <input type="text" name="name" id="name" class="form-control" value="{{$model['name'] or ''}}" readonly="readonly">
            </div>
        </div>
        
                
        <div class="form-group">
            <label for="startDate" class="col-sm-3 control-label">StartDate</label>
            <div class="col-sm-6">
                <input type="text" name="startDate" id="startDate" class="form-control" value="{{$model['startDate'] or ''}}" readonly="readonly">
            </div>
        </div>
        
                
        <div class="form-group">
            <label for="endDate" class="col-sm-3 control-label">EndDate</label>
            <div class="col-sm-6">
                <input type="text" name="endDate" id="endDate" class="form-control" value="{{$model['endDate'] or ''}}" readonly="readonly">
            </div>
        </div>
        
                
        <div class="form-group">
            <label for="deviceId" class="col-sm-3 control-label">DeviceId</label>
            <div class="col-sm-6">
                <input type="text" name="deviceId" id="deviceId" class="form-control" value="{{$model['deviceId'] or ''}}" readonly="readonly">
            </div>
        </div>
        
                
        <div class="form-group">
            <label for="created_at" class="col-sm-3 control-label">Created At</label>
            <div class="col-sm-6">
                <input type="text" name="created_at" id="created_at" class="form-control" value="{{$model['created_at'] or ''}}" readonly="readonly">
            </div>
        </div>
        
                
        <div class="form-group">
            <label for="updated_at" class="col-sm-3 control-label">Updated At</label>
            <div class="col-sm-6">
                <input type="text" name="updated_at" id="updated_at" class="form-control" value="{{$model['updated_at'] or ''}}" readonly="readonly">
            </div>
        </div>
        
        
        <div class="form-group">
            <div class="col-sm-offset-3 col-sm-6">
                <a class="btn btn-default" href="{{ url('/device_assignements') }}"><i class="glyphicon glyphicon-chevron-left"></i> Back</a>
            </div>
        </div>

        </form>
    

    </div>
</div>







@endsection