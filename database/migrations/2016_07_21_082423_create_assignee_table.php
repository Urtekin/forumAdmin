<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAssigneeTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      Schema::create('deviceAssignee', function(Blueprint $table){
          $table->dateTime('dateTime');
          $table->string('imei',16);
          $table->string('deviceid');
          $table->string('tcKimlikNo');
          $table->string('name');
          $table->datetime('start');
          $table->datetime('end');
      });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('deviceAssignee');
    }
}
