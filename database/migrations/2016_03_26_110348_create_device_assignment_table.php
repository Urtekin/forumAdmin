<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDeviceAssignmentTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {

        Schema::create('device_assignements',function(Blueprint $table){
                        $table->increments('id');
                        $table->string('imei',16);
                        $table->string('kimlikNo',11);
                        $table->string('name');
                        $table->datetime('startDate');
                        $table->datetime('endDate');
                        $table->string('deviceId',16);
                        $table->timestamps();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('device_assignements');
    }
}
