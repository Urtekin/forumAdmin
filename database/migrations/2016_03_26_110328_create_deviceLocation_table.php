<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDeviceLocationTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {

        Schema::create('device_locations', function(Blueprint $table){
            $table->dateTime('dateTime');
            $table->string('imei',16);
            $table->string('deviceId',16);
            $table->integer('assignedId');
            $table->double('lat',12,9);
            $table->double('lon',12,9);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('device_locations');
    }
}
