<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCinemasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {

        Schema::create('movies',function(Blueprint $table){
            $table->increments('id');
            $table->string('name',256);
            $table->date('StartDate');
            $table->boolean('active');
        });

        Schema::create('sessions',function(Blueprint $table){
            $table->string('hour',8);
            $table->integer('movieId');
            $table->string('saloon',16);

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('movies');
        Schema::drop('sessions');
    }
}
