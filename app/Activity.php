<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Activity extends Model
{
    
        /**
     * The attributes that are mass assignable.
     *
     * @var string
     */
    protected $table = 'activities';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'title', 'content', 'startDate','endDate'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'id', 'ontheAir','created_at','updated_at'
    ];
}
