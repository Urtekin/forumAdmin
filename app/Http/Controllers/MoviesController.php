<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Movie;

use DB;

class MoviesController extends Controller
{
    //
    public function __construct()
    {
        //$this->middleware('auth');
    }


    public function getIndex(Request $request)
	{
	    return view('movies.index', []);
	}

	public function getAdd(Request $request)
	{
	    return view('movies.add', [
	        []
	    ]);
	}

	public function getUpdate(Request $request, $id)
	{
		$movie = Movie::findOrFail($id);
	    return view('movies.add', [
	        'model' => $movie	    ]);
	}

	public function getShow(Request $request, $id)
	{
		$movie = Movie::findOrFail($id);
	    return view('movies.show', [
	        'model' => $movie	    ]);
	}

	public function getGrid(Request $request)
	{
		$len = $_GET['length'];
		$start = $_GET['start'];

		$select = "SELECT *,1,2 ";
		$presql = " FROM movies a ";
		if($_GET['search']['value']) {	
			$presql .= " WHERE name LIKE '%".$_GET['search']['value']."%' ";
		}
		
		$presql .= "  ";

		$sql = $select.$presql." LIMIT ".$start.",".$len;


		$qcount = DB::select("SELECT COUNT(a.id) c".$presql);
		//print_r($qcount);
		$count = $qcount[0]->c;

		$results = DB::select($sql);
		$ret = [];
		foreach ($results as $row) {
			$r = [];
			foreach ($row as $value) {
				$r[] = $value;
			}
			$ret[] = $r;
		}

		$ret['data'] = $ret;
		$ret['recordsTotal'] = $count;
		$ret['iTotalDisplayRecords'] = $count;

		$ret['recordsFiltered'] = count($ret);
		$ret['draw'] = $_GET['draw'];

		echo json_encode($ret);

	}


	public function postSave(Request $request) {
	    //
	    /*$this->validate($request, [
	        'name' => 'required|max:255',
	    ]);*/
		$movie = null;
		if($request->id > 0) { $movie = Movie::findOrFail($request->id); }
		else { 
			$movie = new Movie;
		}
	    

	    	    $movie->id = $request->id;
	    	    $movie->name = $request->name;
	    	    $movie->StartDate = $request->StartDate;
	    	    $movie->active = $request->active;
	    	    //$movie->user_id = $request->user()->id;
	    $movie->save();

	    return redirect('/movies/index');

	}

	public function getDelete(Request $request, $id) {
		
		$movie = Movie::findOrFail($id);

		$movie->delete();
		return redirect('/movies/index');
	    
	}

	
}
