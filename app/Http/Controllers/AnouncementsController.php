<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Anouncement;

use DB;

class AnouncementsController extends Controller
{
    //
    public function __construct()
    {
        //$this->middleware('auth');
    }


    public function getIndex(Request $request)
	{
	    return view('anouncements.index', []);
	}

	public function getAdd(Request $request)
	{
	    return view('anouncements.add', [
	        []
	    ]);
	}

	public function getUpdate(Request $request, $id)
	{
		$anouncement = Anouncement::findOrFail($id);
	    return view('anouncements.add', [
	        'model' => $anouncement	    ]);
	}

	public function getShow(Request $request, $id)
	{
		$anouncement = Anouncement::findOrFail($id);
	    return view('anouncements.show', [
	        'model' => $anouncement	    ]);
	}

	public function getGrid(Request $request)
	{
		$len = $_GET['length'];
		$start = $_GET['start'];

		$select = "SELECT *,1,2 ";
		$presql = " FROM anouncements a ";
		if($_GET['search']['value']) {	
			$presql .= " WHERE title LIKE '%".$_GET['search']['value']."%' ";
		}
		
		$presql .= "  ";

		$sql = $select.$presql." LIMIT ".$start.",".$len;


		$qcount = DB::select("SELECT COUNT(a.id) c".$presql);
		//print_r($qcount);
		$count = $qcount[0]->c;

		$results = DB::select($sql);
		$ret = [];
		foreach ($results as $row) {
			$r = [];
			foreach ($row as $value) {
				$r[] = $value;
			}
			$ret[] = $r;
		}

		$ret['data'] = $ret;
		$ret['recordsTotal'] = $count;
		$ret['iTotalDisplayRecords'] = $count;

		$ret['recordsFiltered'] = count($ret);
		$ret['draw'] = $_GET['draw'];

		echo json_encode($ret);

	}


	public function postSave(Request $request) {
	    //
	    /*$this->validate($request, [
	        'name' => 'required|max:255',
	    ]);*/
		$anouncement = null;
		if($request->id > 0) { $anouncement = Anouncement::findOrFail($request->id); }
		else { 
			$anouncement = new Anouncement;
		}
	    

	    	    $anouncement->id = $request->id;
	    	    $anouncement->title = $request->title;
	    	    $anouncement->content = $request->content;
	    	    $anouncement->date = $request->date;
	    	    $anouncement->created_at = $request->created_at;
	    	    $anouncement->updated_at = $request->updated_at;
	    	    //$anouncement->user_id = $request->user()->id;
	    $anouncement->save();

	    return redirect('/anouncements/index');

	}

	public function getDelete(Request $request, $id) {
		
		$anouncement = Anouncement::findOrFail($id);

		$anouncement->delete();
		return redirect('/anouncements/index');
	    
	}

	
}
