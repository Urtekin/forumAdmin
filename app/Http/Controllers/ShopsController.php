<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Shop;

use DB;

class ShopsController extends Controller
{
    //
    public function __construct()
    {
        //$this->middleware('auth');
    }


    public function getIndex(Request $request)
	{
	    return view('shops.index', []);
	}

	public function getAdd(Request $request)
	{
	    return view('shops.add', [
	        []
	    ]);
	}

	public function getUpdate(Request $request, $id)
	{
		$shop = Shop::findOrFail($id);
	    return view('shops.add', [
	        'model' => $shop	    ]);
	}

	public function getShow(Request $request, $id)
	{
		$shop = Shop::findOrFail($id);
	    return view('shops.show', [
	        'model' => $shop	    ]);
	}

	public function getGrid(Request $request)
	{
		$len = $_GET['length'];
		$start = $_GET['start'];

		$select = "SELECT *,1,2 ";
		$presql = " FROM shops a ";
		if($_GET['search']['value']) {	
			$presql .= " WHERE name LIKE '%".$_GET['search']['value']."%' ";
		}
		
		$presql .= "  ";

		$sql = $select.$presql." LIMIT ".$start.",".$len;


		$qcount = DB::select("SELECT COUNT(a.id) c".$presql);
		//print_r($qcount);
		$count = $qcount[0]->c;

		$results = DB::select($sql);
		$ret = [];
		foreach ($results as $row) {
			$r = [];
			foreach ($row as $value) {
				$r[] = $value;
			}
			$ret[] = $r;
		}

		$ret['data'] = $ret;
		$ret['recordsTotal'] = $count;
		$ret['iTotalDisplayRecords'] = $count;

		$ret['recordsFiltered'] = count($ret);
		$ret['draw'] = $_GET['draw'];

		echo json_encode($ret);

	}


	public function postSave(Request $request) {
	    //
	    /*$this->validate($request, [
	        'name' => 'required|max:255',
	    ]);*/
		$shop = null;
		if($request->id > 0) { $shop = Shop::findOrFail($request->id); }
		else { 
			$shop = new Shop;
		}
	    

	    	    $shop->id = $request->id;
	    	    $shop->name = $request->name;
	    	    $shop->content = $request->content;
	    	    $shop->created_at = $request->created_at;
	    	    $shop->updated_at = $request->updated_at;
	    	    //$shop->user_id = $request->user()->id;
	    $shop->save();

	    return redirect('/shops/index');

	}

	public function getDelete(Request $request, $id) {
		
		$shop = Shop::findOrFail($id);

		$shop->delete();
		return redirect('/shops/index');
	    
	}

	
}
