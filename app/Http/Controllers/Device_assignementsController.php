<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Device_assignement;
use App\DeviceLocations;

use DB;

class Device_assignementsController extends Controller
{
    //
    public function __construct()
    {
        //$this->middleware('auth');
    }


    public function getIndex(Request $request)
	{
	    return view('device_assignements.index', []);
	}

	public function getAdd(Request $request)
	{
	    return view('device_assignements.add', [
	        []
	    ]);
	}

	public function getUpdate(Request $request, $id)
	{
		$device_assignement = Device_assignement::findOrFail($id);
	    return view('device_assignements.add', [
	        'model' => $device_assignement	    ]);
	}

	public function getShow(Request $request, $id)
	{
		$device_assignement = Device_assignement::findOrFail($id);
	    return view('device_assignements.show', [
	        'model' => $device_assignement	    ]);
	}
  public function getLocations(Request $request,$imei,$start,$end)
  {

    $deviceLocations = DeviceLocations::get();
    return($deviceLocations);
    //dd($deviceLocations);
    //return( $imei . ":" . $start . ":raegharhyar5yruyrst6ustruhstyg" . $end);
  }
	public function getGrid(Request $request)
	{
		$len = $_GET['length'];
		$start = $_GET['start'];

		$select = "SELECT *,1,2 ";
		$presql = " FROM device_assignements a ";
		if($_GET['search']['value']) {
			$presql .= " WHERE imei LIKE '%".$_GET['search']['value']."%' ";
		}

		$presql .= "  ";

		$sql = $select.$presql." LIMIT ".$start.",".$len;


		$qcount = DB::select("SELECT COUNT(a.id) c".$presql);
		//print_r($qcount);
		$count = $qcount[0]->c;

		$results = DB::select($sql);
		$ret = [];
		foreach ($results as $row) {
			$r = [];
			foreach ($row as $value) {
				$r[] = $value;
			}
			$ret[] = $r;
		}

		$ret['data'] = $ret;
		$ret['recordsTotal'] = $count;
		$ret['iTotalDisplayRecords'] = $count;

		$ret['recordsFiltered'] = count($ret);
		$ret['draw'] = $_GET['draw'];

		echo json_encode($ret);

	}


	public function postSave(Request $request) {
	    //
	    /*$this->validate($request, [
	        'name' => 'required|max:255',
	    ]);*/
		$device_assignement = null;
		if($request->id > 0) { $device_assignement = Device_assignement::findOrFail($request->id); }
		else {
			$device_assignement = new Device_assignement;
		}


	    	    $device_assignement->id = $request->id;
	    	    $device_assignement->imei = $request->imei;
	    	    $device_assignement->kimlikNo = $request->kimlikNo;
	    	    $device_assignement->name = $request->name;
	    	    $device_assignement->startDate = $request->startDate;
	    	    $device_assignement->endDate = $request->endDate;
	    	    $device_assignement->deviceId = $request->deviceId;
	    	    $device_assignement->created_at = $request->created_at;
	    	    $device_assignement->updated_at = $request->updated_at;
	    	    //$device_assignement->user_id = $request->user()->id;
	    $device_assignement->save();

	    return redirect('/device_assignements/index');

	}

	public function getDelete(Request $request, $id) {

		$device_assignement = Device_assignement::findOrFail($id);

		$device_assignement->delete();
		return redirect('/device_assignements/index');

	}


}
