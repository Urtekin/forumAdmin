<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Session;

use DB;

class SessionsController extends Controller
{
    //
    public function __construct()
    {
        //$this->middleware('auth');
    }


    public function getIndex(Request $request)
	{
	    return view('sessions.index', []);
	}

	public function getAdd(Request $request)
	{
	    return view('sessions.add', [
	        []
	    ]);
	}

	public function getUpdate(Request $request, $id)
	{
		$session = Session::findOrFail($id);
	    return view('sessions.add', [
	        'model' => $session	    ]);
	}

	public function getShow(Request $request, $id)
	{
		$session = Session::findOrFail($id);
	    return view('sessions.show', [
	        'model' => $session	    ]);
	}

	public function getGrid(Request $request)
	{
		$len = $_GET['length'];
		$start = $_GET['start'];

		$select = "SELECT *,1,2 ";
		$presql = " FROM sessions a ";
		if($_GET['search']['value']) {	
			$presql .= " WHERE movieId LIKE '%".$_GET['search']['value']."%' ";
		}
		
		$presql .= "  ";

		$sql = $select.$presql." LIMIT ".$start.",".$len;


		$qcount = DB::select("SELECT COUNT(a.id) c".$presql);
		//print_r($qcount);
		$count = $qcount[0]->c;

		$results = DB::select($sql);
		$ret = [];
		foreach ($results as $row) {
			$r = [];
			foreach ($row as $value) {
				$r[] = $value;
			}
			$ret[] = $r;
		}

		$ret['data'] = $ret;
		$ret['recordsTotal'] = $count;
		$ret['iTotalDisplayRecords'] = $count;

		$ret['recordsFiltered'] = count($ret);
		$ret['draw'] = $_GET['draw'];

		echo json_encode($ret);

	}


	public function postSave(Request $request) {
	    //
	    /*$this->validate($request, [
	        'name' => 'required|max:255',
	    ]);*/
		$session = null;
		if($request->id > 0) { $session = Session::findOrFail($request->id); }
		else { 
			$session = new Session;
		}
	    

	    	    $session->hour = $request->hour;
	    	    $session->movieId = $request->movieId;
	    	    $session->saloon = $request->saloon;
	    	    //$session->user_id = $request->user()->id;
	    $session->save();

	    return redirect('/sessions/index');

	}

	public function getDelete(Request $request, $id) {
		
		$session = Session::findOrFail($id);

		$session->delete();
		return redirect('/sessions/index');
	    
	}

	
}
