<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Activity;

use DB;

class ActivitiesController extends Controller
{
    //
    public function __construct()
    {
        //$this->middleware('auth');
    }


    public function getIndex(Request $request)
	{
	    return view('activities.index', []);
	}

	public function getAdd(Request $request)
	{
	    return view('activities.add', [
	        []
	    ]);
	}

	public function getUpdate(Request $request, $id)
	{
		$activity = Activity::findOrFail($id);
	    return view('activities.add', [
	        'model' => $activity	    ]);
	}

	public function getShow(Request $request, $id)
	{
		$activity = Activity::findOrFail($id);
	    return view('activities.show', [
	        'model' => $activity	    ]);
	}

	public function getGrid(Request $request)
	{
		$len = $_GET['length'];
		$start = $_GET['start'];

		$select = "SELECT *,1,2 ";
		$presql = " FROM activities a ";
		if($_GET['search']['value']) {	
			$presql .= " WHERE title LIKE '%".$_GET['search']['value']."%' ";
		}
		
		$presql .= "  ";

		$sql = $select.$presql." LIMIT ".$start.",".$len;


		$qcount = DB::select("SELECT COUNT(a.id) c".$presql);
		//print_r($qcount);
		$count = $qcount[0]->c;

		$results = DB::select($sql);
		$ret = [];
		foreach ($results as $row) {
			$r = [];
			foreach ($row as $value) {
				$r[] = $value;
			}
			$ret[] = $r;
		}

		$ret['data'] = $ret;
		$ret['recordsTotal'] = $count;
		$ret['iTotalDisplayRecords'] = $count;

		$ret['recordsFiltered'] = count($ret);
		$ret['draw'] = $_GET['draw'];

		echo json_encode($ret);

	}


	public function postSave(Request $request) {
	    //
	    /*$this->validate($request, [
	        'name' => 'required|max:255',
	    ]);*/
		$activity = null;
		if($request->id > 0) { $activity = Activity::findOrFail($request->id); }
		else { 
			$activity = new Activity;
		}
	    

	    	    $activity->id = $request->id;
	    	    $activity->title = $request->title;
	    	    $activity->content = $request->content;
	    	    $activity->ontheAir = $request->ontheAir;
	    	    $activity->startDate = $request->startDate;
	    	    $activity->endDate = $request->endDate;
	    	    $activity->created_at = $request->created_at;
	    	    $activity->updated_at = $request->updated_at;
	    	    //$activity->user_id = $request->user()->id;
	    $activity->save();

	    return redirect('/activities/index');

	}

	public function getDelete(Request $request, $id) {
		
		$activity = Activity::findOrFail($id);

		$activity->delete();
		return redirect('/activities/index');
	    
	}

	
}
