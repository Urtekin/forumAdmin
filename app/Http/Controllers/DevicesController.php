<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Carbon\Carbon;
use App\Device;
use App\DeviceAssignements;
use App\DeviceLocations;
use DB;

class DevicesController extends Controller
{
    //
    public function __construct()
    {
        //$this->middleware('auth');
    }


    public function getIndex(Request $request)
	{
	    return view('devices.index', []);
	}

	public function getAdd(Request $request)
	{
	    return view('devices.add', [
	        []
	    ]);
	}
  public function getHistory(Request $request,$id)
  {
    $model = Device::findorFail($id);
    return view('devices.history',[
      'model' => $model
    ]);
  }
  public function getHistoryGrid(Request $request,$deviceId)
  {

    //----------
    $len = $_GET['length'];
    $start = $_GET['start'];

    $select = "SELECT id,deviceId,kimlikNo,name,startDate,endDate ";
    $presql = " FROM device_assignements a where deviceId like " . $deviceId;
    if($_GET['search']['value']) {
      $presql .= " WHERE kimlikNo LIKE '%".$_GET['search']['value']."%' ";
      $presql .= " or name LIKE '%".$_GET['search']['value']."%' ";
    }

    $presql .= "  ";

    $sql = $select.$presql." LIMIT ".$start.",".$len;


    $qcount = DB::select("SELECT COUNT(a.id) c".$presql);
    //print_r($qcount);
    $count = $qcount[0]->c;

    $results = DB::select($sql);
    $ret = [];
    foreach ($results as $row) {
      $r = [];
      foreach ($row as $value) {
        $r[] = $value;
      }
      $ret[] = $r;
    }

    $ret['data'] = $ret;
    $ret['recordsTotal'] = $count;
    $ret['iTotalDisplayRecords'] = $count;

    $ret['recordsFiltered'] = count($ret);
    $ret['draw'] = $_GET['draw'];

    echo json_encode($ret);
//---------------
  }
  public function getAssignee(Request $request,$id)
  {
    //dd($request);
    //$anouncement = Anouncement::with('Pictures')->orderBy('id','desc')->findOrFail($id);
    $device = Device::findorFail($id);
    $assignement = NULL;
    if($device->assignedId != "-1")
      $assignement = DeviceAssignements::findorFail($device->assignedId);
    //dd($model);
    return view('devices.assign',[
      'device' => $device,
      'assignement' => $assignement
    ]);
  }
  public function getWhereis(Request $request,$id)
  {
    $device = Device::findorFail($id);
    return view('devices.map',[
      'model' => $device
    ]);
  }
	public function getUpdate(Request $request, $id)
	{
		$device = Device::findOrFail($id);
	    return view('devices.add', [
	        'model' => $device	    ]);
	}

	public function getShow(Request $request, $id)
	{
		$device = Device::findOrFail($id);
	    return view('devices.show', [
	        'model' => $device	    ]);
	}

	public function getGrid(Request $request)
	{
		$len = $_GET['length'];
		$start = $_GET['start'];

		$select = "SELECT *,1,2 ";
		$presql = " FROM devices a ";
		if($_GET['search']['value']) {
			$presql .= " WHERE imei LIKE '%".$_GET['search']['value']."%' ";
		}

		$presql .= "  ";

		$sql = $select.$presql." LIMIT ".$start.",".$len;


		$qcount = DB::select("SELECT COUNT(a.id) c".$presql);
		//print_r($qcount);
		$count = $qcount[0]->c;

		$results = DB::select($sql);
		$ret = [];
		foreach ($results as $row) {
			$r = [];
			foreach ($row as $value) {
				$r[] = $value;
			}
			$ret[] = $r;
		}

		$ret['data'] = $ret;
		$ret['recordsTotal'] = $count;
		$ret['iTotalDisplayRecords'] = $count;

		$ret['recordsFiltered'] = count($ret);
		$ret['draw'] = $_GET['draw'];

		echo json_encode($ret);

	}

  public function postAssigneeSave(Request $request)
  {
    $device = null;
    $deviceAssignement = null;
    if($request->deviceId > 0){
      $device = Device::findOrFail($request->deviceId);
    }
    //if($request->kimlikNo)
  }
	public function postSave(Request $request) {
	    //
	    /*$this->validate($request, [
	        'name' => 'required|max:255',
	    ]);*/
		$device = null;
		if($request->id > 0) { $device = Device::findOrFail($request->id); }
		else {
			$device = new Device;
		}
	    	    $device->id = $request->id;
	    	    $device->imei = $request->imei;
	    	    $device->deviceId = $request->deviceId;
            $device->assignedId = "-1";
            $device->created_at = $request->created_at;
            $device->updated_at = $request->updated_at;
	    	    //$device->user_id = $request->user()->id;
	    $device->save();

	    return redirect('/devices/index');

	}

	public function getDelete(Request $request, $id) {

		$device = Device::findOrFail($id);

		$device->delete();
		return redirect('/devices/index');

	}
  public function getLastloc(Request $request, $deviceId)
  {
    //DB::enableQueryLog();
    $deviceLoc = DeviceLocations::where('deviceId',$deviceId)
                          ->where('lat','>','1')
                          ->where('lon','>','1')
                          ->orderBy('dateTime','desc')
                          ->first();
    //dd(DB::getQueryLog());
    echo json_encode($deviceLoc);
  }
  public function getHistloc(Request $request, $assignedId)
  {
    //dd($assignedId);
    $deviceLocs = DeviceLocations::where('assignedId',$assignedId)->get();
    echo json_encode($deviceLocs);
    //return $deviceLocs;
  }
  public function postAssign(Request $request,$id)
  {
    $assignement = new DeviceAssignements;
    $assignement->kimlikNo = $request->kimlikno;
    $assignement->name = $request->name;
    $assignement->imei = $request->imei;
    $assignement->deviceId = $request->deviceId;
    $assignement->startDate = carbon::now();
    $assignement->save();
    $device = Device::findOrFail($id);
    $device->assignedId = $assignement->id;
    $device->save();
    return redirect('/devices/index');
  }
  public function getDeleteassign(Request $request,$id)
  {
    $device = Device::findOrFail($id);

    $assignement = NULL;
    $assignement = DeviceAssignements::findorFail($device->assignedId);
    $assignement->endDate = carbon::now();
    $assignement->save();
    $device->assignedId = "-1";
    $device->save();

    return view('devices.assign',[
      'device' => $device,
      'assignement'=> NULL
    ]);


  }


}
