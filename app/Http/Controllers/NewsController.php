<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\News;

use DB;

class NewsController extends Controller
{
    //
    public function __construct()
    {
        //$this->middleware('auth');
    }


    public function getIndex(Request $request)
	{
	    return view('news.index', []);
	}

	public function getAdd(Request $request)
	{
	    return view('news.add', [
	        []
	    ]);
	}

	public function getUpdate(Request $request, $id)
	{
		$news = News::findOrFail($id);
	    return view('news.add', [
	        'model' => $news	    ]);
	}

	public function getShow(Request $request, $id)
	{
		$news = News::findOrFail($id);
	    return view('news.show', [
	        'model' => $news	    ]);
	}

	public function getGrid(Request $request)
	{
		$len = $_GET['length'];
		$start = $_GET['start'];

		$select = "SELECT *,1,2 ";
		$presql = " FROM news a ";
		if($_GET['search']['value']) {	
			$presql .= " WHERE title LIKE '%".$_GET['search']['value']."%' ";
		}
		
		$presql .= "  ";

		$sql = $select.$presql." LIMIT ".$start.",".$len;


		$qcount = DB::select("SELECT COUNT(a.id) c".$presql);
		//print_r($qcount);
		$count = $qcount[0]->c;

		$results = DB::select($sql);
		$ret = [];
		foreach ($results as $row) {
			$r = [];
			foreach ($row as $value) {
				$r[] = $value;
			}
			$ret[] = $r;
		}

		$ret['data'] = $ret;
		$ret['recordsTotal'] = $count;
		$ret['iTotalDisplayRecords'] = $count;

		$ret['recordsFiltered'] = count($ret);
		$ret['draw'] = $_GET['draw'];

		echo json_encode($ret);

	}


	public function postSave(Request $request) {
	    //
	    /*$this->validate($request, [
	        'name' => 'required|max:255',
	    ]);*/
		$news = null;
		if($request->id > 0) { $news = News::findOrFail($request->id); }
		else { 
			$news = new News;
		}
	    

	    	    $news->id = $request->id;
	    	    $news->title = $request->title;
	    	    $news->content = $request->content;
	    	    $news->created_at = $request->created_at;
	    	    $news->updated_at = $request->updated_at;
	    	    $news->active = $request->active;
	    	    //$news->user_id = $request->user()->id;
	    $news->save();

	    return redirect('/news/index');

	}

	public function getDelete(Request $request, $id) {
		
		$news = News::findOrFail($id);

		$news->delete();
		return redirect('/news/index');
	    
	}

	
}
