<?php

/*
|--------------------------------------------------------------------------
| Routes File
|--------------------------------------------------------------------------
|
| Here is where you will register all of the routes in an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/', function () {
    return view('welcome');
});
Route::get('/admin','AdminController@dashBoardView');
Route::get('/map','AdminController@mapView');


//Route::get('/dashboard','AdminController@dashBoard');

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| This route group applies the "web" middleware group to every route
| it contains. The "web" middleware group is defined in your HTTP
| kernel and includes session state, CSRF protection, and more.
|
*/

Route::group(['middleware' => ['web']], function () {
    //
});

Route::controller('/activities', 'ActivitiesController');
Route::controller('/anouncements', 'AnouncementsController');
Route::controller('/devices', 'DevicesController');
Route::controller('/movies', 'MoviesController');
Route::controller('/news', 'NewsController');
Route::controller('/shops', 'ShopsController');
Route::controller('/sessions', 'SessionsController');
Route::controller('/device_assignements', 'Device_assignementsController');