<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Device extends Model
{
    //
    public function assignement()
    {
      //return $this->hasMany('App\Comment', 'foreign_key');
      //return $this->hasOne('App\Phone', 'foreign_key', 'local_key');
      return $this->hasOne('App\DeviceAssignements','id','assignedId');
    }
    public function locationList()
    {
      return $this->hasMany('App\deviceLocation','imei','imei');
    }
}
